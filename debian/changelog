fonts-georgewilliams (20031023-3) unstable; urgency=medium

  * debian/control
    - Drop obsolete dependency with ttf-georgewilliams (Closes: #878489)
    - Remove Christian Perrier <bubulle@debian.org> from Uploaders and
      set me as it
    - Set Standards-Version: 4.5.0
    - Drop debhelper, then set  debhelper-compat (= 13)
    - Update Vcs-* to poin salsa.debian.org
    - Add Rules-Requires-Root: no
    - Remove Homepage: URL, it is disappeared.
  * debian/rules
    - Use default compression
    - Split to debian/clean
  * debian/copyright
    - Use https
    - Update copyright year
    - Describe GPL as explicitly GPL-2+
  * Drop debian/compat
  * Drop obsolete debian/maintscript
  * Add debian/salsa-ci.yml

 -- Hideki Yamane <henrich@debian.org>  Sun, 14 Jun 2020 13:12:15 +0900

fonts-georgewilliams (20031023-2) unstable; urgency=low

  * Team upload.
  * Changed font name from Monospace to GWMonospace to avoid overriding
    system's default monospace font. Closes: #760826

 -- Francesca Ciceri <madamezou@debian.org>  Wed, 10 Sep 2014 18:50:33 +0200

fonts-georgewilliams (20031023-1) unstable; urgency=low

  * Team upload
  * Use maintscript support in dh_installdeb rather than writing out
    dpkg-maintscript-helper commands by hand.  We now simply Pre-Depend on a
    new enough version of dpkg rather than using 'dpkg-maintscript-helper
    supports' guards, leading to more predictable behaviour on upgrade.
    Thanks Colin Watson for the patch. Closes: #659716
  * Use git for packaging: added Vcs-Git and Vcs-Browser fields
  * Add Multi-Arch: foreign field
  * Rename source package to "fonts-georgewilliams" to fit the Font Packages
    Naming Policy
  * bumped debhelper compatibility
  * bumped standard version to 3.9.5 (checked)
  * new release, based on most recent upstream version. Closes: #434624

 -- Francesca Ciceri <madamezou@debian.org>  Wed, 14 May 2014 17:20:47 +0200

gw-fonts-ttf (1.0-5) unstable; urgency=low

  * Adopt this package (team maintenance)
  * Change section to fonts
  * Bump Standards to 3.9.2
  * Drop Recommends on x-ttcidfont-conf | fontconfig
  * Drop defoma support (*this* is what this is all about...:-))
  * Add ${misc:Depends} to package dependencies
  * Move to source v3 format
  * Bump debhelper compatibility level to 8 and use a minimal rules file

 -- Christian Perrier <bubulle@debian.org>  Thu, 21 Jul 2011 18:20:23 +0200

gw-fonts-ttf (1.0-4) unstable; urgency=low

  * QA upload.
  * Remove fc-cache call from postinst (Closes: #533418)

 -- Filippo Giunchedi <filippo@debian.org>  Thu, 25 Jun 2009 16:07:20 +0200

gw-fonts-ttf (1.0-3) unstable; urgency=low

  * Orphaning package, setting maintainer to the Debian QA Group.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Wed, 05 Dec 2007 15:36:20 +0100

gw-fonts-ttf (1.0-2) unstable; urgency=low

  * Do not install the monospace font. (Closes: #395487)
  * debian/control:
    + Updated and moved homepage field.
    + Updated long description.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Wed, 24 Oct 2007 11:00:21 +0200

gw-fonts-ttf (1.0-1) unstable; urgency=low

  * Initial release. (Closes: #391662)

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Tue, 10 Oct 2006 00:10:03 +0200
